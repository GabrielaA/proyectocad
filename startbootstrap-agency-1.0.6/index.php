<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>ITA (Innovaciones Tecnológicas Avanzadas)</title>

	<!-- Bootstrap Core CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet">

	<!--Referencia a imagen.css -->
	<link href="css/imagen.css" rel="stylesheet">

	<!-- Custom CSS -->
	<link href="css/agency.css" rel="stylesheet">

	<!-- Custom Fonts -->
	<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
	<link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>

<body id="page-top" class="index">

	<!-- Navigation -->
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header page-scroll">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div class="logo">
					<img src="img/ITA.png" width="60px" height="40px">
					<a href="#page-top">INNOVACIONES TECNOLÓGICAS AVANZADAS</a>
				</div>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li class="hidden">
						<a href="#page-top"></a>
					</li>
					<li>
						<a class="page-scroll" href="#register">Registro</a>
					</li>
					<li>
						<a class="page-scroll" href="buscarPerfil.php">Buscar</a>
					</li>
				</ul>
				</div>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>

	<!-- Header -->
	<header>
		<div class="container">
			<div class="intro-text">
				<div class="intro-heading">Bienvenido</div>
				<div class="intro-lead-in">Búsqueda de perfiles</div>
				<a href="buscarPerfil.php" class="page-scroll btn btn-xl">Buscar&nbsp;&nbsp;<span class="glyphicon glyphicon-search" aria-hidden="true"></span></a>
			</div>
		</div>
	</header>   

	<!-- Registro seccion -->

	<section id="register" class="bg-light-gray">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<h2 class="section-heading">Sección de registro de perfiles</h2>
					<h3 class="section-subheading text-muted">Introduzca los datos para registrar un nuevo perfil.</h3>
				</div>

				<!-- Form de los atributos a introducir por perfil -->
				<form align="center" action="registro.php" method="POST" enctype="multipart/form-data">
					<div  class="form-group col-md-4 col-md-offset-4">

							
					<!-- Combobox donde se obtiene la clasificación de los  --> 
					<label>Clasificación*:</label>
						<!-- <select name="select_clasificacion" id="select_clasificacion" class="form-control"> -->
						<select name="clasif" id="clasif" class="form-control" onchange="buscarEscuela();">
						 	<option value=""> Seleccione una clasificación</option>
							<?php 

								$link = mysql_connect('localhost', 'cad', 'hola123') //Conexion a la BD(servidor,usuario,contraseña)
										or die('No se pudo conectar: ' . mysql_error()); //Mensaje de no conexion
									mysql_select_db('perfiles_db') or die('No se pudo seleccionar la base de datos');//Se selecciona el nombre de la BD

								$sql="SELECT id_clasif, nombre_clasif FROM clasif_esc"; //Pide el atributo nombre_clasif de la tabla clasif_esc
								$rs=mysql_query($sql, $link)or die ("Error: " .mysql_error(). "con el query: " . $sql);//Manda los argumentos del query sql y la conexion a la BD


								//Ciclo donde row es una variable la cual va a guardar un array con los 
								//valores que tiene rs y echo imprime los valores del array en forma de lista
								
								while ($row=mysql_fetch_array($rs)) 
								{ 
									echo "<option value='".$row[0]."'>".$row[1]."</option>"; 
								} 
							?> 
						</select>


						
						<!--Combobox donde se van a obtener los nombres de las escuelas de acuerdo a la clasificación -->
						<label>Escuela*:</label>
						<select name="escu" id="escu" class="form-control">
							<option value="">Selecciona primero una clasificación</option>
							<!-- <option value="saab">Saab</option>
							<option value="mercedes">Mercedes</option>
							<option value="audi">Audi</option>  -->
						</select>
					   

						

						<label>Apellido Paterno*:</label>
						<input type="text" name="apaterno" class="form-control" placeholder="Hernández" required/> 

						<label for="apellido">Apellido Materno*:</label>
						<input type="text" name="amaterno" class="form-control" placeholder="López" required/>

						<label for="nombre">Nombre (s)*:</label>
						<input type="text" name="nombre" class="form-control" placeholder="Jorge" required/> 

						<label for="nombre">Fecha de Nacimiento*:</label>
						<input type="date" name="fechaNac" class="form-control" placeholder="04/10/1994" required/>  

						<label>Sexo*:</label>
						<select class="form-control" name="select_sexo">
							<option value="femenino">Femenino</option>
							<option value="masculino"> Masculino</option>
						</select>

						<label for="email">Email*:</label>
						<input type="email" name="email" class="form-control" placeholder="prueba@gmail.com" />

						<label for="email">Facebook*:</label>
						<input type="text" name="fb" class="form-control" placeholder="JorgeHernandez"/>

						<label for="email">Twitter*:</label>
						<input type="text" name="tw" class="form-control" placeholder="Jorge21"/>

						<label for="nombre">Usuario*:</label>
						<input type="text" name="usuario" class="form-control" placeholder="FernandaAO" required/> 

						<label for="nombre">Clave*:</span></label>
						<input type="password" name="clave" class="form-control" required/> 

						<label for="nombre">Foto*:</span></label>
						<input type="file" name="foto" id="foto" required/> 
						<br>
						<br>
						<center> <input class="btn btn-primary" name="submit" type="submit" value="Registrar" /></center>
					</div> 
				</form>
			</div>
		</div>
	</section>



	<!-- Buscar Seccion -->
	<!-- <section id="team" class="bg-light-gray">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<h2 class="section-heading">Búsqueda de perfiles</h2>
					<h3 class="section-subheading text-muted">Introduzca el nombre del perfil a búscar.</h3>
				</div>

				<form align="center" action="buscarPerfil.php" method="POST" class="form-inline">
					<div  class="form-group">
						<label>Institución*:</label>
						<select class="form-control">
							<option value="volvo">Volvo</option>
							<option value="saab">Saab</option>
							<option value="mercedes">Mercedes</option>
							<option value="audi">Audi</option>
						</select>

						<label>Escuela*:</label>
						<select class="form-control">
							<option value="volvo">Volvo</option>
							<option value="saab">Saab</option>
							<option value="mercedes">Mercedes</option>
							<option value="audi">Audi</option>
						</select>

						<label>Usuario : </label>
						<input type="text" class="form-control" name="nombre_usuario" id="nombre_usuario" placeholder="Juan Pérez">

					</div>
					<button type="submit" class="btn btn-primary">Buscar</button>
				</form>
			</div>
			<br>
			<br>
			<br>
			
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2 text-center">
					<p class="large text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut eaque, laboriosam veritatis, quos non quis ad perspiciatis, totam corporis ea, alias ut unde.</p>
				</div>
			</div>
		</div>
	</section> -->
	

	<!-- <footer>
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<span class="copyright">Copyright &copy; Your Website 2014</span>
				</div>
				<div class="col-md-4">
					<ul class="list-inline social-buttons">
						<li><a href="#"><i class="fa fa-twitter"></i></a>
						</li>
						<li><a href="#"><i class="fa fa-facebook"></i></a>
						</li>
						<li><a href="#"><i class="fa fa-linkedin"></i></a>
						</li>
					</ul>
				</div>
				<div class="col-md-4">
					<ul class="list-inline quicklinks">
						<li><a href="#">Privacy Policy</a>
						</li>
						<li><a href="#">Terms of Use</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</footer> -->

	<!-- Portfolio Modals -->
	<!-- Use the modals below to showcase details about your portfolio projects! -->

	<!-- Portfolio Modal 1 -->
	<div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-content">
			<div class="close-modal" data-dismiss="modal">
				<div class="lr">
					<div class="rl">
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-lg-offset-2">
						<div class="modal-body">
							<!-- Project Details Go Here -->
							<h2>Project Name</h2>
							<p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
							<img class="img-responsive img-centered" src="img/portfolio/roundicons-free.png" alt="">
							<p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
							<p>
								<strong>Want these icons in this portfolio item sample?</strong>You can download 60 of them for free, courtesy of <a href="https://getdpd.com/cart/hoplink/18076?referrer=bvbo4kax5k8ogc">RoundIcons.com</a>, or you can purchase the 1500 icon set <a href="https://getdpd.com/cart/hoplink/18076?referrer=bvbo4kax5k8ogc">here</a>.</p>
							<ul class="list-inline">
								<li>Date: July 2014</li>
								<li>Client: Round Icons</li>
								<li>Category: Graphic Design</li>
							</ul>
							<button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Portfolio Modal 2 -->
	<div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-content">
			<div class="close-modal" data-dismiss="modal">
				<div class="lr">
					<div class="rl">
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-lg-offset-2">
						<div class="modal-body">
							<h2>Project Heading</h2>
							<p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
							<img class="img-responsive img-centered" src="img/portfolio/startup-framework-preview.png" alt="">
							<p><a href="http://designmodo.com/startup/?u=787">Startup Framework</a> is a website builder for professionals. Startup Framework contains components and complex blocks (PSD+HTML Bootstrap themes and templates) which can easily be integrated into almost any design. All of these components are made in the same style, and can easily be integrated into projects, allowing you to create hundreds of solutions for your future projects.</p>
							<p>You can preview Startup Framework <a href="http://designmodo.com/startup/?u=787">here</a>.</p>
							<button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Portfolio Modal 3 -->
	<div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-content">
			<div class="close-modal" data-dismiss="modal">
				<div class="lr">
					<div class="rl">
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-lg-offset-2">
						<div class="modal-body">
							<!-- Project Details Go Here -->
							<h2>Project Name</h2>
							<p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
							<img class="img-responsive img-centered" src="img/portfolio/treehouse-preview.png" alt="">
							<p>Treehouse is a free PSD web template built by <a href="https://www.behance.net/MathavanJaya">Mathavan Jaya</a>. This is bright and spacious design perfect for people or startup companies looking to showcase their apps or other projects.</p>
							<p>You can download the PSD template in this portfolio sample item at <a href="http://freebiesxpress.com/gallery/treehouse-free-psd-web-template/">FreebiesXpress.com</a>.</p>
							<button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Portfolio Modal 4 -->
	<div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-content">
			<div class="close-modal" data-dismiss="modal">
				<div class="lr">
					<div class="rl">
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-lg-offset-2">
						<div class="modal-body">
							<!-- Project Details Go Here -->
							<h2>Project Name</h2>
							<p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
							<img class="img-responsive img-centered" src="img/portfolio/golden-preview.png" alt="">
							<p>Start Bootstrap's Agency theme is based on Golden, a free PSD website template built by <a href="https://www.behance.net/MathavanJaya">Mathavan Jaya</a>. Golden is a modern and clean one page web template that was made exclusively for Best PSD Freebies. This template has a great portfolio, timeline, and meet your team sections that can be easily modified to fit your needs.</p>
							<p>You can download the PSD template in this portfolio sample item at <a href="http://freebiesxpress.com/gallery/golden-free-one-page-web-template/">FreebiesXpress.com</a>.</p>
							<button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Portfolio Modal 5 -->
	<div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-content">
			<div class="close-modal" data-dismiss="modal">
				<div class="lr">
					<div class="rl">
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-lg-offset-2">
						<div class="modal-body">
							<!-- Project Details Go Here -->
							<h2>Project Name</h2>
							<p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
							<img class="img-responsive img-centered" src="img/portfolio/escape-preview.png" alt="">
							<p>Escape is a free PSD web template built by <a href="https://www.behance.net/MathavanJaya">Mathavan Jaya</a>. Escape is a one page web template that was designed with agencies in mind. This template is ideal for those looking for a simple one page solution to describe your business and offer your services.</p>
							<p>You can download the PSD template in this portfolio sample item at <a href="http://freebiesxpress.com/gallery/escape-one-page-psd-web-template/">FreebiesXpress.com</a>.</p>
							<button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Portfolio Modal 6 -->
	<div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-content">
			<div class="close-modal" data-dismiss="modal">
				<div class="lr">
					<div class="rl">
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-lg-offset-2">
						<div class="modal-body">
							<!-- Project Details Go Here -->
							<h2>Project Name</h2>
							<p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
							<img class="img-responsive img-centered" src="img/portfolio/dreams-preview.png" alt="">
							<p>Dreams is a free PSD web template built by <a href="https://www.behance.net/MathavanJaya">Mathavan Jaya</a>. Dreams is a modern one page web template designed for almost any purpose. It’s a beautiful template that’s designed with the Bootstrap framework in mind.</p>
							<p>You can download the PSD template in this portfolio sample item at <a href="http://freebiesxpress.com/gallery/dreams-free-one-page-web-template/">FreebiesXpress.com</a>.</p>
							<button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- jQuery -->
	<script src="js/jquery.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.min.js"></script>

	<!-- Plugin JavaScript -->
	<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
	<script src="js/classie.js"></script>
	<script src="js/cbpAnimatedHeader.js"></script>

	<!-- Contact Form JavaScript -->
	<script src="js/jqBootstrapValidation.js"></script>
	<script src="js/contact_me.js"></script>

	<!-- Custom Theme JavaScript -->
	<script src="js/agency.js"></script>
	<script>
		function buscarEscuela(){
				// document.getElementById('clasif').style.display='none';
			var j = jQuery.noConflict();
			// j(document).ready(function(){
				// $("#clasif").change(function(){
					debugger
				// j("select").change(function(){
					// Vector para saber cuál es el siguiente combo a llenar
					var combos = new Array();
					combos['clasif'] = "escu";
					// Tomo el nombre del combo al que se le a dado el clic
					//posicion = j('#clasif').val();
					valor = j('#clasif').val();
					// Tomo el valor de la opción seleccionada
					//valor = j(this).val()
					posicion='clasif';
					// Evaluó  que si el 'padre' esta seleccionado y el valor es 0, vacíe los 'hijos'
					if(valor==0){
						j("#escu").html('    <option value="0" selected="selected">Seleccione primero el estado</option>')
					}else{
						/* En caso contrario agregado el letreo de cargando a el combo siguiente
							Ejemplo: Si seleccione país voy a tener que el siguiente según mi vector combos es: estado  por qué  combos [país] = estado
						*/
						//j("#"+combos[posicion]).html('<option selected="selected" value="0">Cargando...</option>')
						/* Verificamos si el valor seleccionado es diferente de 0*/
						if(valor!="0"){
							// Llamamos a pagina de combos.php donde ejecuto las consultas para llenar los combos
							j.post("getEscuela.php",{
								combo:j(this).attr("name"), // Nombre del combo
								id:valor // Valor seleccionado
							},function(data){
								j("#"+combos[posicion]).html(data);    //Tomo el resultado de pagina e inserto los datos en el combo indicado                                                                               
							})                                               
						}
					}
				// });
			// });


		}
		
	</script>

</body>

</html>
